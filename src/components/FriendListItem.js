import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import styles from './FriendListItem.css';

export default class FriendListItem extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    starred: PropTypes.bool,
    editPhone: PropTypes.bool,
    phone: PropTypes.string.isRequired,
    editFriend: PropTypes.func.isRequired,
    deleteFriend: PropTypes.func.isRequired
  }

  saveFriend (e){
    console.log(this.props.editPhone);
    if (e.which === 13) {
      this.props.editFriend(this.props.id);
    }
  }

  render () {
    return (
      <li className={styles.friendListItem}>
        <div className={styles.friendInfos}>

          {this.props.starred ?
            <div><input value={this.props.name} onChange={() => this.props.editFriendName(this.props.id, event.target.value)} onKeyUp={this.saveFriend.bind(this)}></input></div>
          :
            <div><span>{this.props.name}</span></div>
          }
          <div>
            {this.props.editPhone ?
            <small onClick={() => this.props.editPhone(this.props.id)}>{this.props.phone}</small>
            :
            <input value={this.props.phone}></input>
            }
          </div>
        </div>

        <div className={styles.friendActions}>
          <button className={`btn btn-default ${styles.btnAction}`} onClick={() => this.props.editFriend(this.props.id)}>
            <i className={classnames('fa', { 'fa-pencil-square-o': this.props.starred }, { 'fa-pencil-square': !this.props.starred })} />
          </button>
          <button className={`btn btn-default ${styles.btnAction}`} onClick={() => this.props.deleteFriend(this.props.id)}>
            <i className="fa fa-trash" />
          </button>
        </div>
      </li>
    );
  }

}
