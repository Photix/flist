import * as types from '../constants/ActionTypes';

export function addFriend(name) {
  return {
    type: types.ADD_FRIEND,
    name
  };
}

export function deleteFriend(id) {
  return {
    type: types.DELETE_FRIEND,
    id
  };
}

export function editFriend(id) {
  console.log('edit friend here');
  return {
    type: types.EDIT_FRIEND,
    id
  };
}

export function editFriendName(id, value) {
  return {
    type: types.EDIT_FRIEND_NAME,
    id,
    value
  };
}

export function editPhone(id) {
  return {
    type: types.EDIT_PHONE,
    id
  };
}

export function editPhoneValue(id, value) {
  return {
    type: types.EDIT_PHONE_VALUE,
    id,
    value
  };
}
