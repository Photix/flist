import * as types from '../constants/ActionTypes';
import omit from 'lodash/object/omit';
import assign from 'lodash/object/assign';
import mapValues from 'lodash/object/mapValues';

const initialState = {
  friends: [1, 2, 3],
  friendsById: {
    1: {
      id: 1,
      name: 'Theodore Roosevelt',
      phone: '+38095003002'
    },
    2: {
      id: 2,
      name: 'Abraham Lincoln',
      phone: '+38095003002'
    },
    3: {
      id: 3,
      name: 'George Washington',
      phone: '+38095003002'
    }
  }
};

export default function friends(state = initialState, action) {
  switch (action.type) {

    case types.ADD_FRIEND:
      const newId = state.friends[state.friends.length-1] + 1;
      return {
        ...state,
        friends: state.friends.concat(newId),
        friendsById: {
          ...state.friendsById,
          [newId]: {
            id: newId,
            name: action.name
          }
        },
      }

    case types.DELETE_FRIEND:
      return {
        ...state,
        friends: state.friends.filter(id => id !== action.id),
        friendsById: omit(state.friendsById, action.id)
      }

    case types.EDIT_FRIEND:
      return {
        ...state,
        friendsById: mapValues(state.friendsById, (friend) => {
          return friend.id === action.id ?
            assign({}, friend, { starred: !friend.starred}) :
            friend
        })
      }

    case types.EDIT_FRIEND_NAME:
      return {
        ...state,
        friendsById: mapValues(state.friendsById, (friend) => {
          return friend.id === action.id ?
            assign({}, friend, { name: action.value}) :
            friend
        })
      }

    case types.EDIT_PHONE:
    return {
      ...state,
      friendsById: mapValues(state.friendsById, (friend) => {
        return friend.id === action.id ?
          assign({}, friend, { editPhone: !friend.editPhone}) :
          friend
      })
    }

    case types.EDIT_PHONE_VALUE:
    return {
      ...state,
      friendsById: mapValues(state.friendsById, (friend) => {
        return friend.id === action.id ?
          assign({}, friend, { phone: action.value}) :
          friend
      })
    }

    default:
      return state;
  }
}
